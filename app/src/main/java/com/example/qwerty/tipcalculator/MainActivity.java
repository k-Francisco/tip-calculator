package com.example.qwerty.tipcalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener, TextWatcher {

    private SeekBar mSeekBar;
    private TextView mPercentage, mTip, mTotal;
    private EditText mBill, mNumberOfPeople;
    int tip;
    private float bill = 0, people = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        mSeekBar = (SeekBar)findViewById(R.id.seekBar);
        mPercentage = (TextView)findViewById(R.id.tvTipPercentage);
        mTip = (TextView)findViewById(R.id.tvTip);
        mTotal = (TextView)findViewById(R.id.tvTotal);
        mBill = (EditText)findViewById(R.id.etBill);
        mNumberOfPeople = (EditText)findViewById(R.id.etNumberOfPeople);

        mSeekBar.setOnSeekBarChangeListener(this);
        mBill.addTextChangedListener(this);
        mNumberOfPeople.addTextChangedListener(this);

    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        mPercentage.setText(mSeekBar.getProgress() + "%");
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        tip = mSeekBar.getProgress();
        displayCalculation();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        try {
                bill = Float.parseFloat(String.valueOf(mBill.getText()));
            people = Float.parseFloat(String.valueOf(mNumberOfPeople.getText()));
            displayCalculation();
        }
        catch (Exception e){
            mTotal.setText("Php 0.00");
            mTip.setText("Php 0.00");
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private void displayCalculation() {
        float tipPerson;
        tipPerson = (float)tip/100;
            mTotal.setText("Php " + String.valueOf((bill / people) + ((bill / people) * tipPerson)));
            mTip.setText("Php" +String.valueOf((bill / people) * tipPerson));

    }


}
